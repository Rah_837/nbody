#version 330 core


uniform sampler2D tex;

in vec2 v_tex_coords;



void main()
{
    gl_FragColor = texture(tex, v_tex_coords);
}