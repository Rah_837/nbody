#version 330 core


in vec2 position;
in vec2 tex_coords;

out vec2 v_tex_coords;



void main()
{
    gl_Position = vec4(position, 0.0f, 1.0f);

    v_tex_coords = tex_coords;
}