#[macro_use]
extern crate glium;
use glium::{
    glutin,
    texture,
    index::PrimitiveType
};

extern crate singly_linked_list as list;
use list::SinglyLinkedList as List;

extern crate image;

extern crate rand;
use rand::Rng;

use std::{
    fs::File,
    io::{
        Read,
        Cursor
    }
};

mod vector;
use vector::{
    Vector2f,
    Vector2u
};

mod vertex;
use vertex::Vertex;

mod body;
use body::Body;

mod mesh;
use mesh::{
    build_vertex_buffer,
    build_index_buffer
};



fn main() {
    implement_vertex!(Vertex, position, tex_coords);

    let mut events_loop = glutin::EventsLoop::new();
    let display = {
        let window = glutin::WindowBuilder::new()
            .with_fullscreen(Some(events_loop.get_primary_monitor()))
            .with_title("N-Body");
        let context = glutin::ContextBuilder::new()
            .with_vsync(true);
        glium::Display::new(window, context, &events_loop).unwrap()
    };
    let display_size = Vector2u::from(display.gl_window().get_inner_size().unwrap());

    let mut map: List<_> = {
        let mut rng = rand::thread_rng();
        (0..16u8).map(|_| Body::new(Vector2f::new(  rng.gen_range(0., display_size.x.into()),
                                                    rng.gen_range(0., display_size.y.into())), rng.gen_range(1., 40.))).collect()
    };
    
    let texture = {
        let image = image::load(    Cursor::new(&include_bytes!("../resources/body.png")[..]),
                                    image::PNG).unwrap().to_rgba();
        let dimensions = image.dimensions();
        let image = texture::RawImage2d::from_raw_rgba_reversed(&image.into_raw(), dimensions);
        texture::CompressedSrgbTexture2d::new(&display, image).unwrap()
    };

    let program = {
        let mut v_sh_src = String::new();
        File::open("shaders/vertex.glsl").unwrap()
            .read_to_string(&mut v_sh_src).unwrap();

        let mut f_sh_src = String::new();
        File::open("shaders/fragment.glsl").unwrap()
            .read_to_string(&mut f_sh_src).unwrap();

        glium::Program::from_source(&display, v_sh_src.as_str(), f_sh_src.as_str(), None).unwrap()
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &build_vertex_buffer(&map, display_size)[..]).unwrap();

    let index_buffer = glium::IndexBuffer::new(&display, PrimitiveType::TrianglesList, &build_index_buffer(map.len())[..]).unwrap();
    
    let params = glium::DrawParameters {
        blend: glium::draw_parameters::Blend::alpha_blending(),
        .. Default::default()
    };


    let mut done = false;
    while !done {
        use glium::Surface;

        let mut target = display.draw();
        target.clear_color(0.015, 0., 0.03, 0.);
        target.draw(&vertex_buffer, &index_buffer, &program, &uniform! {
            tex: &texture
        }, &params).unwrap();
        target.finish().unwrap();

        events_loop.poll_events(|event| {
            use glutin::{ Event, WindowEvent, ElementState, VirtualKeyCode };

            match event {
                Event::WindowEvent { event, window_id } =>
                    if window_id == display.gl_window().id() {
                        match event {
                            WindowEvent::Closed => done = true,
                            WindowEvent::KeyboardInput { input, .. } => {
                                match input.state {
                                    ElementState::Released => match input.virtual_keycode {
                                        Some(VirtualKeyCode::Escape) => done = true,
                                        _ => ()
                                    }
                                    _ => ()
                                }
                            }
                            _ => ()
                        }
                    }
                _ => ()
            }
        });
    }
}