use super::{
    Vector2f,
    Vector2u,
    Vertex,
    List,
    Body
};



#[inline]
pub fn build_index_buffer(length: usize) -> Vec<u16> {
    let mut indices = Vec::with_capacity(length * 6);

    for i in 0..length {
        let i = i as u16;

        indices.push(i * 4 + 1);
        indices.push(i * 4 + 2);
        indices.push(i * 4 + 0);

        indices.push(i * 4 + 2);
        indices.push(i * 4 + 0);
        indices.push(i * 4 + 3);
    }

    indices
}

#[inline]
pub fn build_vertex_buffer(map: &List<Body>, display_size: Vector2u) -> Vec<Vertex> {
    let mut vertices = Vec::with_capacity(map.len() * 4);


    for body in map {
        let position = body.get_position();
        let radius = body.get_radius();

        vertices.push(Vertex::new(
            Vertex::display_coords_to_nds(Vector2f::new(position.x - radius, position.y + radius), display_size),
            Vector2f::new(0., 0.)));

        vertices.push(Vertex::new(
            Vertex::display_coords_to_nds(Vector2f::new(position.x - radius, position.y - radius), display_size),
            Vector2f::new(0., 1.)));

        vertices.push(Vertex::new(
            Vertex::display_coords_to_nds(Vector2f::new(position.x + radius, position.y - radius), display_size),
            Vector2f::new(1., 1.)));

        vertices.push(Vertex::new(
            Vertex::display_coords_to_nds(Vector2f::new(position.x + radius, position.y + radius), display_size),
            Vector2f::new(1., 0.)));
    }

    vertices
}