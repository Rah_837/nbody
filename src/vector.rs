use std::ops::{
    Neg,
    Sub, SubAssign,
    Add, AddAssign,
    Div, DivAssign,
    Mul, MulAssign
};



#[derive(Eq, PartialEq, Copy, Clone, Default, Debug)]
pub struct Vector2<T> {
    pub x: T,
    pub y: T
}

pub type Vector2f = Vector2<f64>;
pub type Vector2u = Vector2<u32>;
pub type Vector2i = Vector2<i32>;

impl<T> Vector2<T> {
    #[inline]
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}



impl<T> Neg for Vector2<T>
    where T: Neg<Output = T> {
    type Output = Self;

    #[inline]
    fn neg(self) -> Self::Output {
        Self::Output {
            x: -self.x,
            y: -self.y
        }
    }
}


impl<T> Sub for Vector2<T>
    where T: Sub<Output = T> {
    type Output = Self;

    #[inline]
    fn sub(self, other: Self) -> Self::Output {
        Self::Output {
            x: self.x - other.x,
            y: self.y - other.y
        }
    }
}

impl<T> SubAssign for Vector2<T>
    where T: SubAssign {
    #[inline]
    fn sub_assign(&mut self, other: Self) {
        self.x -= other.x;
        self.y -= other.y;
    }
}


impl<T> Add for Vector2<T>
    where T: Add<Output = T> {
    type Output = Self;

    #[inline]
    fn add(self, other: Self) -> Self::Output {
        Self::Output {
            x: self.x + other.x,
            y: self.y + other.y
        }
    }
}

impl<T> AddAssign for Vector2<T>
    where T: AddAssign {
    #[inline]
    fn add_assign(&mut self, other: Self) {
        self.x += other.x;
        self.y += other.y;
    }
}


impl<T> Div for Vector2<T>
    where T: Div<Output = T> {
    type Output = Self;

    #[inline]
    fn div(self, other: Self) -> Self::Output {
        Self::Output {
            x: self.x / other.x,
            y: self.y / other.y
        }
    }
}

impl<T> DivAssign for Vector2<T>
    where T: DivAssign {
    #[inline]
    fn div_assign(&mut self, other: Self) {
        self.x /= other.x;
        self.y /= other.y;
    }
}


impl<T> Mul for Vector2<T>
    where T: Mul<Output = T> {
    type Output = Self;

    #[inline]
    fn mul(self, other: Self) -> Self::Output {
        Self::Output {
            x: self.x * other.x,
            y: self.y * other.y
        }
    }
}

impl<T> MulAssign for Vector2<T>
    where T: MulAssign {
    #[inline]
    fn mul_assign(&mut self, other: Self) {
        self.x *= other.x;
        self.y *= other.y;
    }
}




impl<T> Sub<T> for Vector2<T>
    where T: Sub<Output = T> + Clone {
    type Output = Self;

    #[inline]
    fn sub(self, other: T) -> Self::Output {
        Self::Output {
            x: self.x - other.clone(),
            y: self.y - other
        }
    }
}

impl<T> SubAssign<T> for Vector2<T>
    where T: SubAssign + Clone {
    #[inline]
    fn sub_assign(&mut self, other: T) {
        self.x -= other.clone();
        self.y -= other;
    }
}


impl<T> Add<T> for Vector2<T>
    where T: Add<Output = T> + Clone {
    type Output = Self;

    #[inline]
    fn add(self, other: T) -> Self::Output {
        Self::Output {
            x: self.x + other.clone(),
            y: self.y + other
        }
    }
}

impl<T> AddAssign<T> for Vector2<T>
    where T: AddAssign + Clone {
    #[inline]
    fn add_assign(&mut self, other: T) {
        self.x += other.clone();
        self.y += other;
    }
}


impl<T> Div<T> for Vector2<T>
    where T: Div<Output = T> + Clone {
    type Output = Self;

    #[inline]
    fn div(self, other: T) -> Self::Output {
        Self::Output {
            x: self.x / other.clone(),
            y: self.y / other
        }
    }
}

impl<T> DivAssign<T> for Vector2<T>
    where T: DivAssign + Clone {
    #[inline]
    fn div_assign(&mut self, other: T) {
        self.x /= other.clone();
        self.y /= other;
    }
}


impl<T> Mul<T> for Vector2<T>
    where T: Mul<Output = T> + Clone {
    type Output = Self;

    #[inline]
    fn mul(self, other: T) -> Self::Output {
        Self::Output {
            x: self.x * other.clone(),
            y: self.y * other
        }
    }
}

impl<T> MulAssign<T> for Vector2<T>
    where T: MulAssign + Clone {
    #[inline]
    fn mul_assign(&mut self, other: T) {
        self.x *= other.clone();
        self.y *= other;
    }
}




impl<T1, T2> From<(T1, T1)> for Vector2<T2>
    where T2: From<T1> {
    #[inline]
    fn from(tuple: (T1, T1)) -> Self {
        Self { 
            x: tuple.0.into(),
            y: tuple.1.into()
        }
    }
}




#[macro_export]
macro_rules! impl_From_for_Vector2 {
    ($($T1:ty => $T2:ty),*) => {$(  
        impl From<Vector2<$T1>> for Vector2<$T2> {
            #[inline]
            fn from(vector: Vector2<$T1>) -> Self {
                Self { 
                    x: vector.x.into(),
                    y: vector.y.into()
                }
            }
        }
    )*}
}

impl_From_for_Vector2!(
    u32 => u64,
    i32 => i64,
    f32 => f64,

    u32 => f64
);