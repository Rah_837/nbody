use super::{
    Vector2f,
    Vector2u,
    glium::vertex
};



#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pub position: Vector2f,
    pub tex_coords: Vector2f
}

impl Vertex {
    #[inline]
    pub fn new(position: Vector2f, tex_coords: Vector2f) -> Self {
        Self { position, tex_coords }
    }

    #[inline]
    pub fn display_coords_to_nds(coords: Vector2f, size: Vector2u) -> Vector2f {
        let size = Vector2f::from(size);

        Vector2f::new(  coords.x * 2. / size.x - 1.,
                      -(coords.y * 2. / size.y - 1.))
    }
}

unsafe impl vertex::Attribute for Vector2f {
    #[inline]
    fn get_type() -> vertex::AttributeType {
        vertex::AttributeType::F64F64
    }
}