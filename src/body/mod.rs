use super::Vector2f;

use std::mem;

mod math;
use self::math::{
    area_from_radius,
    radius_from_area
};



#[derive(PartialEq, Clone, Default, Debug)]
pub struct Body {
    position: Vector2f,
    velocity: Vector2f,
    radius: f64,

    force: Vector2f,
    d_area: f64,

    removed: bool
}

impl Body {
    #[inline]
    pub fn new(position: Vector2f, radius: f64) -> Self {
        Self {
            position,
            velocity: Vector2f::new(0., 0.),
            radius,

            force: Vector2f::new(0., 0.),
            d_area: 0.,

            removed: false
        }
    }

    #[inline]
    pub fn update(&mut self) {
        let area = area_from_radius(self.radius);

        self.velocity += mem::replace(&mut self.force, Vector2f::new(0., 0.)) / area;
        self.position += self.velocity;
        self.radius = radius_from_area(area + self.d_area);
    }

    #[inline]
    pub fn remove(&mut self) {
        self.removed = true;
    }

    #[inline]
    pub fn is_removed(&self) -> bool {
        self.removed
    }

    #[inline]
    pub fn add_force(&mut self, force: f64) {
        self.force += force;
    }

    #[inline]
    pub fn add_radius(&mut self, radius: f64) {
        self.d_area += area_from_radius(radius);
    }

    #[inline]
    pub fn get_position(&self) -> Vector2f {
        self.position
    }

    #[inline]
    pub fn get_radius(&self) -> f64 {
        self.radius
    }
}