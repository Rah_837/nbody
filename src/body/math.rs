use std::f64::consts;



#[inline]
pub fn area_from_radius(radius: f64) -> f64 {
    consts::PI * radius.powi(2)
}

#[inline]
pub fn radius_from_area(area: f64) -> f64 {
    (area / consts::PI).sqrt()
}